FROM "golang:1.20.1-alpine3.17"
RUN apk add docker-compose 
RUN apk add gettext
RUN go install github.com/adnanh/webhook@latest
COPY predefined/webhook/init /init
CMD /init